import tensorflow as tf
from tensorflow.contrib.rnn import HighwayWrapper, LSTMCell, DropoutWrapper
from tensorflow.python.ops import array_ops

PARALLEL_ITERATIONS = 32




def orthonorm(shape, dtype=tf.float32,   partition_info=None): 
    if len(shape) != 2 or shape[0] != shape[1]:
        raise ValueError(" Exception_occured %s" % shape)
    _, u, _ = tf.svd(tf.random_normal(shape, dtype=dtype), full_matrices=True)
    return u


def _reverse(input_, seq_lengths, seq_dim, batch_dim):  # reverses sequences
    return array_ops.reverse_sequence(
        input=input_, seq_lengths=seq_lengths,
        seq_dim=seq_dim, batch_dim=batch_dim)


def bidirectional_lstms_stacked(inputs, num_layers, size, keep_prob, lengths):
    outputs = inputs
    for layer in range(num_layers // 2):
        with tf.variable_scope('bilstm_{}'.format(layer), reuse=tf.AUTO_REUSE):
            cell_fw = HighwayWrapper(DropoutWrapper(LSTMCell(size), variational_recurrent=True, dtype=tf.float32, state_keep_prob=keep_prob))
            cell_bw = HighwayWrapper(DropoutWrapper(LSTMCell(size),variational_recurrent=True, dtype=tf.float32, state_keep_prob=keep_prob))
            (output_fw, output_bw), states = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw,   outputs, sequence_length=lengths,dtype=tf.float32)
            outputs = tf.add(output_fw, output_bw)  
    return outputs


def bidirectional_lstms_interleaved(inputs, num_layers, size, keep_prob, lengths):
    outputs = inputs
    for layer in range(num_layers):
        print('Layer {}: Creating {} LSTM NET'.format(layer, 'backw.' if layer % 2 else 'forw.'))  # backward
        with tf.variable_scope('bilstm_{}'.format(layer)):

            cell = HighwayWrapper(DropoutWrapper(LSTMCell(size),
                                                 variational_recurrent=True,
                                                 dtype=tf.float32,
                                                 state_keep_prob=keep_prob))

            if layer % 2:
                outputs_reverse = _reverse(outputs, seq_lengths=lengths, seq_dim=1, batch_dim=0)
                tmp, _ = tf.nn.dynamic_rnn(cell=cell,
                                           inputs=outputs_reverse,
                                           sequence_length=lengths,
                                           dtype=tf.float32)
                outputs = _reverse(tmp, seq_lengths=lengths, seq_dim=1, batch_dim=0)
            else:
                outputs, _ = tf.nn.dynamic_rnn(cell=cell,
                                               inputs=outputs,
                                               sequence_length=lengths,
                                               dtype=tf.float32)

    return outputs


class Model():
    def __init__(self, config, embeddings, num_labels):


        with tf.device('/cpu:0'):
            self.word_ids = tf.placeholder(tf.int32, [None, None])
            x_embedded = tf.nn.embedding_lookup(embeddings, self.word_ids)


        with tf.device('/gpu:0'):        #  bi lstm
            self.predicate_ids = tf.placeholder(tf.float32, [None, None])
            self.keep_prob = tf.placeholder(tf.float32, name='keep_prob')
            self.lengths = tf.placeholder(tf.int32, [None])
            inputs = tf.concat([x_embedded, tf.expand_dims(self.predicate_ids, -1)], axis=2)

            if config.architecture == 'stacked':
                final_outputs = bidirectional_lstms_stacked(inputs, config.num_layers, config.cell_size, self.keep_prob, self.lengths)
            elif config.architecture == 'interleaved':
                final_outputs = bidirectional_lstms_interleaved(inputs, config.num_layers, config.cell_size, self.keep_prob, self.lengths)
            else:
                raise AttributeError('Invalid situation!"')

            # projection part
            shape0 = tf.shape(final_outputs)[0] * tf.shape(final_outputs)[1]  
            final_outputs_2d = tf.reshape(final_outputs, [shape0, config.cell_size])  
            wy = tf.get_variable('Wy', [config.cell_size, num_labels])
            by = tf.get_variable('by', [num_labels])
            self.logits = tf.matmul(final_outputs_2d, wy) + by  

            # loss part
            self.label_ids = tf.placeholder(tf.int32, [None, None]) 
            label_ids_flat = tf.reshape(self.label_ids, [-1]) 
            losses = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.logits, labels=label_ids_flat)
            mask = tf.sign(tf.to_float(label_ids_flat)) 
            masked_losses = mask * losses
            self.mean_loss = tf.reduce_mean(masked_losses)  

            # update part
            optimizer = tf.train.AdadeltaOptimizer(learning_rate=config.learning_rate, epsilon=config.epsilon)
            gradients, variables = zip(*optimizer.compute_gradients(self.mean_loss))
            gradients, _ = tf.clip_by_global_norm(gradients, config.max_grad_norm)
            self.update = optimizer.apply_gradients(zip(gradients, variables))

            # for metrics
            self.predictions = tf.reshape(tf.argmax(tf.nn.softmax(self.logits), axis=1), tf.shape(self.label_ids))
