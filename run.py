# #!/usr/bin/env/python3

import json
from argparse import Namespace
import tensorflow as tf
import time
import argparse

from data_utils import get_data
from train_utils import get_feed_dicts, evaluate, shuffle_stack_pad
from model import Model

TRAIN_DATA_PATH = 'data/conll05.train.txt'
DEV_DATA_PATH = 'data/conll05.dev.txt'

LOSS_INTERVAL = 100
TFVIS_PATH = '/home/ph'


def srl_task(config_file_path):

    # config
    with open(config_file_path, 'r') as config_file:
        config = json.load(config_file, object_hook=lambda d: Namespace(**d))
    print('///// Configs START')
    for k in sorted(config.__dict__):
        v = vars(config)[k]
        print('    {:>20}={:<20}'.format(k, v))
    print('///// Configs END')

    # data
    train_data, dev_data, word_dict, label_dict, embeddings = get_data(
        config, TRAIN_DATA_PATH, DEV_DATA_PATH)

    g = tf.Graph()
    with g.as_default():
        model = Model(config, embeddings, label_dict.size())
        local_step = 0
        train_loss = 0.0
        global_start = time.time()
        with tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)) as sess:
            sess.run(tf.global_variables_initializer())
            for epoch in range(config.max_epochs):
                # eval
                evaluate(dev_data, config.dev_batch_size, config, model, sess, epoch, word_dict, label_dict)
                # train
                x1, x2, y = shuffle_stack_pad(train_data, config.train_batch_size)
                epoch_start = time.time()
                for feed_dict in get_feed_dicts(x1, x2, y, model, config.train_batch_size, config.keep_prob):
                    loss, _ = sess.run([model.mean_loss, model.update], feed_dict=feed_dict)
                    train_loss += loss
                    local_step += 1
                    if local_step % LOSS_INTERVAL == 0 or local_step == 1:
                        print("step {:>6} epoch {:>3}: loss={:1.3f}, epoch sec={:3.0f}, total hrs={:.1f}".format(
                            local_step,
                            epoch,
                            train_loss / local_step,
                            (time.time() - epoch_start),
                            (time.time() - global_start) / 3600))
                local_step = 0
                train_loss = 0.0


def getIndex(entry):
    if entry == 0:
        return 'A0'
    elif entry == 1:
        return 'A1'
    elif entry == 2:
        return 'A2'
    elif entry == 3:
        return 'A3'
    elif entry == 4:
        return 'A4'
    elif entry == 5:
        return 'AM-LOC'
    elif entry == 6:
        return 'AM-MNR'
    elif entry == 7:
        return 'AM-MOD'
    elif entry == 8:
        return 'AM-NEG'
    elif entry == 9:
        return 'AM-TMP'
    elif entry == 10:
        return '_'


fin = open('answer.txt', 'w')
#fin.write("%s\n" % result[:count+1])
# fin.close()
result = []
predlabel_count = 0
count = 0
for sentence in keepoutput:  # for ecery sentence
    predlabel_count = predlabel_count + count
    count = 0
    for i in range(len(sentence)):
        if sentence[i][12] == 'Y':
            count = count + 1

    result = []
    for column_num in range(len(sentence)):
        result.append(sentence[column_num])
        # fin.write(str(sentence[column_num]))
        fin.write('\t'.join(str(x) for x in list(sentence[column_num])))
        fin.write('\t')
        for c in range(count):
            result.append(generetedLabels2[predlabel_count + c][column_num])
            # fin.write(str(generetedLabels2[predlabel_count+c][column_num]))
            fin.write(getIndex(generetedLabels2[predlabel_count + c][column_num]))
            fin.write('\t')
        fin.write('\n')
    fin.write('\n')

fin.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', action="store", default=0, dest='config_number', type=int,
                        help='Number of config_file (e.g. "1" in config1.json)')
    namespace = parser.parse_args()
    config_file_path = 'configs/' + 'config{}.json'.format(namespace.config_number)
    srl_task(config_file_path)
